schema = Dry::Validation.Form do
  required(:age).maybe(:int?)
end

context 'Schema validation' do
  m_yaml("Drama", "dramas", schema)
end
